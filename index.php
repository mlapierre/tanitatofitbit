<?php

error_reporting(E_ALL);

ini_set("display_errors", 1);

require __DIR__ . '/vendor/autoload.php';
require 'tanitaparser.php';

use djchen\OAuth2\Client\Provider\Fitbit;

$stage = $_GET['stage'];
$_SESSION['stage'] = $stage;

$provider = new Fitbit([
    'clientId'          => getenv('OAUTH_CLIENT_ID'),
    'clientSecret'      => getenv('OAUTH_CLIENT_SECRET'),
    'redirectUri'       => getenv('OAUTH_REDIRECT_URI_INDEX')]
);

// start the session
session_start();

// If we don't have an authorization code then get one
if (!isset($_GET['code'])) {
  $options = [
    'scope' => ['weight']
  ];

    // Fetch the authorization URL from the provider; this returns the
    // urlAuthorize option and generates and applies any necessary parameters
    // (e.g. state).
    $authorizationUrl = $provider->getAuthorizationUrl($options);

    // Get the state generated for you and store it to the session.
    $_SESSION['oauth2state'] = $provider->getState();

    // Redirect the user to the authorization URL.
    header('Location: ' . $authorizationUrl);
    exit;

// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || array_key_exists('oauth2state', $_SESSION) && ($_GET['state'] !== $_SESSION['oauth2state'])) {
    unset($_SESSION['oauth2state']);
    exit('Invalid state');

} else {

    try {

        // Try to get an access token using the authorization code grant.
        $accessToken = $provider->getAccessToken('authorization_code', [
            'code' => $_GET['code']
        ]);

        // We have an access token, which we may use in authenticated
        // requests against the service provider's API.
        echo $accessToken->getToken() . "\n";
        echo $accessToken->getRefreshToken() . "\n";
        echo $accessToken->getExpires() . "\n";
        echo ($accessToken->hasExpired() ? 'expired' : 'not expired') . "\n";

        // Using the access token, we may look up details about the
        // resource owner.
      /*  $resourceOwner = $provider->getResourceOwner($accessToken);

        var_export($resourceOwner->toArray());*/

        // The provider provides a way to get an authenticated API request for
        // the service, using the access token; it returns an object conforming
        // to Psr\Http\Message\RequestInterface.
      /*  $request = $provider->getAuthenticatedRequest(
            Fitbit::METHOD_GET,
            Fitbit::BASE_FITBIT_API_URL . '/1/user/-/profile.json',
            $accessToken,
            ['headers' => [Fitbit::HEADER_ACCEPT_LANG => 'en_US'], [Fitbit::HEADER_ACCEPT_LOCALE => 'en_US']]
            // Fitbit uses the Accept-Language for setting the unit system used
            // and setting Accept-Locale will return a translated response if available.
            // https://dev.fitbit.com/docs/basics/#localization
        );
        // Make the authenticated API request and get the parsed response.
        $response = $provider->getParsedResponse($request);*/





        // If you would like to get the response headers in addition to the response body, use:
        //$response = $provider->getResponse($request);
        //$headers = $response->getHeaders();
        //$parsedResponse = $provider->parseResponse($response);

    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

        // Failed to get the access token or user details.
        exit($e->getMessage());

    }

}

function showUploadResult() {

  foreach (parseTanitaCSV("DATA1.CSV") as $entry => $measureArray) {
    postWeight($provider, $accessToken, $measureArray["timestamp"], $measureArray["weight"]);
    postFat($provider, $accessToken, $measureArray["timestamp"], $measureArray["fat"]);
  }
}


function postFat($provider, $accessToken, $timestamp, $fat) {

    $parameters = "fat=$fat&date=". date_format($timestamp, "Y-m-d") . "&time=". date_format($timestamp, "H:m:s")."&source=tanita";

  $fatRequest = $provider->getAuthenticatedRequest(
    Fitbit::METHOD_POST,
    Fitbit::BASE_FITBIT_API_URL . '/1/user/-/body/log/fat.json' . "&" . $parameters,
    $accessToken,
    ['headers' => [Fitbit::HEADER_ACCEPT_LANG => 'de_DE'],
      [Fitbit::HEADER_ACCEPT_LOCALE => 'de_DE']
    ]
  );
  $fatResponse = $provider->getParsedResponse($fatRequest);
  echo $fatResponse;
}

function postWeight($provider, $accessToken, $timestamp, $weight) {
  $parameters = "weight=$weight&date=". date_format($timestamp, "Y-m-d") . "&time=". date_format($timestamp, "H:m:s")."&source=tanita";
  $weightRequest = $provider->getAuthenticatedRequest(
    Fitbit::METHOD_POST,
    Fitbit::BASE_FITBIT_API_URL . '/1/user/-/body/log/weight.json' . "&" . $parameters,
    $accessToken,
    ['headers' => [Fitbit::HEADER_ACCEPT_LANG => 'de_DE'],
      [Fitbit::HEADER_ACCEPT_LOCALE => 'de_DE'] /*,
      ['body' => "weight=$weight&date=". date_format($timestamp, "Y-m-d") . "&time=". date_format($timestamp, "HH:mm:ss")
      ]*/
    ]
  );

  $weightResponse = $provider->getParsedResponse($weightRequest);
  echo $weightResponse;
}
?>
